import { combineReducers } from "redux";
import FlightListReducer from "./flightListReducer";

const commonRootReducers = combineReducers({
  Flight: FlightListReducer,
});

export default commonRootReducers;
