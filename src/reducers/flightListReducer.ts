import { IFilterComponentView } from "../components/searchBar/presentational/filterComponentView";


export interface IState {
  list: any;
  search: string;
  loader: boolean;
  filter: IFilterComponentView;
}

const initialState = {
  list: undefined,
  search: "",
  loader: false,
  filter: {},
} as IState;

interface IAction {
  type: string;
  payload: any;
}

const setSearchVal = (state: IState, action: IAction) => {
  return {
    ...state,
    search: action.payload,
  };
};

const setLoaderStatus = (state: IState, action: IAction) => {
  return {
    ...state,
    loader: action.payload,
  };
};

const setFilterVal = (state: IState, action: IAction) => {
  return {
    ...state,
    filter: action.payload,
  };
};

const FlightListReducer = (
  state = initialState,
  action: { type: string; payload: IAction }
) => {
  switch (action.type) {
    case "SET_FLIGHT_LIST":
      return {
        ...state,
        list: action.payload,
      };
    case "SET_SEARCH_VAL":
      return setSearchVal(state, action);
    case "SET_LOADER_STATUS":
      return setLoaderStatus(state, action);
    case "SET_FILTER_VAL":
      return setFilterVal(state, action);
    default:
      return state;
  }
};


export default FlightListReducer;
