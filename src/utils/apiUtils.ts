const API_URL = process.env.REACT_APP_SERVER_URL;

export const getAPIURL = () => {
    return API_URL;
} 