import { ICardModel } from "../components/card/iCardModel";
import moment from "moment";
import { filterTypeEnum } from "../components/searchBar/filterTypeEnum";

interface IQueryParam {
  q: string;
  searchKey: string;
  launchDate: string;
  launchStatus: string;
  upComing: string;
}

export const filteredQueryList = (
  list: ICardModel[],
  queryParams: IQueryParam
) => {
  const { q, searchKey, launchDate, launchStatus, upComing } = queryParams;
  let filteredList: ICardModel[] = list.slice();
  if (q) {
    filteredList = getSearchList(list, searchKey, q);
  }
  if (launchDate) {
    filteredList = getLaunchDateList(filteredList, launchDate);
  }
  if (launchStatus) {
    filteredList = getLaunchStatusList(filteredList, launchStatus);
  }
  if (upComing) {
    filteredList = getUpComingList(filteredList, upComing);
  }
  return filteredList;
};

const getLaunchStatusList = (list: ICardModel[], launchStatus: string) => {
  const isSuccess = filterTypeEnum.SUCCESS === launchStatus ? true : false;
  return list.filter((e) => e.launch_success === isSuccess);
};

const getUpComingList = (list: ICardModel[], upComing: string) => {
  const isUpComing = filterTypeEnum.YES === upComing ? true : false;
  return list.filter((e: ICardModel) => e.upcoming === isUpComing);
};

const getSearchList = (list: ICardModel[], searchKey: string, q: string) => {
  return list.filter((e: any) => e.rocket[searchKey].indexOf(q) !== -1);
};

const getLaunchDateList = (list: ICardModel[], launchDate: string) => {
  const now = moment();
  const currentYear = now.year();
  const currentWeek = now.isoWeek();
  const currentMonth = now.month();
  switch (launchDate) {
    case filterTypeEnum.LAST_WEEK:
      return lastWeekLaunchDateList(list, currentWeek - 1, currentYear);
    case filterTypeEnum.LAST_MONTH:
      return lastMonthLaunchDateList(list, currentMonth - 1, currentYear);
    case filterTypeEnum.LAST_YEAR:
      return lastYearLaunchDateList(list, currentYear - 1);
    default:
      return list;
  }
};

const lastWeekLaunchDateList = (
  list: ICardModel[],
  lastWeek: number,
  currentYear: number
) => {
  return list.filter((e: ICardModel) => {
    if (Number(e.launch_year) === currentYear) {
      const eDate = moment(e.launch_date_utc);
      return Number(lastWeek) === eDate.isoWeek();
    }
    return false;
  });
};

const lastMonthLaunchDateList = (
  list: ICardModel[],
  lastMonth: number,
  currentYear: number
) => {
  return list.filter((e: ICardModel) => {
    if (Number(e.launch_year) === currentYear) {
      const eDate = moment(e.launch_date_utc);
      return Number(lastMonth) === eDate.month();
    }
    return false;
  });
};

const lastYearLaunchDateList = (list: ICardModel[], lastYear: number) => {
  return list.filter((e: ICardModel) => {
    return lastYear === Number(e.launch_year);
  });
};
