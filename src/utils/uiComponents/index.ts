import {
  Grid,
  Card,
  makeStyles,
  CardContent,
  Typography,
  CardActions,
  Button,
  CircularProgress,
  FormControl,
  InputLabel,
  Select,
  MenuItem
} from "@material-ui/core";

let classes: any;
const useStyles = makeStyles((theme) => ({
  root: {
    background: "green",
    border: 0,
    color: "white",
    height: 48,
    padding: "20px 10px",
  },
}));
const StyleComponent = () => {
  classes = useStyles();
};
export {
  Grid,
  StyleComponent,
  classes,
  Card,
  CardContent,
  Typography,
  CardActions,
  Button,
  CircularProgress,
  FormControl,
  InputLabel,
  Select,
  MenuItem
};
