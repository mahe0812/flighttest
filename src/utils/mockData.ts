import { IState } from "../reducers/flightListReducer";

export const flight = {
    "flight_number": 1,
    "mission_name": "FalconSat",
    "mission_id": [],
    "upcoming": false,
    "launch_year": "2006",
    "launch_date_unix": 1143239400,
    "launch_date_utc": "2006-03-24T22:30:00.000Z",
    "launch_date_local": "2006-03-25T10:30:00+12:00",
    "is_tentative": false,
    "tentative_max_precision": "hour",
    "tbd": false,
    "launch_window": 0,
    "rocket": {
      "rocket_id": "falcon1",
      "rocket_name": "Falcon 1",
      "rocket_type": "Merlin A",
      "first_stage": {
        "cores": [
          {
            "core_serial": "Merlin1A",
            "flight": 1,
            "block": "test",
            "gridfins": false,
            "legs": false,
            "reused": false,
            "land_success": "test",
            "landing_intent": false,
            "landing_type": "test",
            "landing_vehicle": "test"
          }
        ]
      },
      "second_stage": {
        "block": 1,
        "payloads": [
          {
            "payload_id": "FalconSAT-2",
            "norad_id": [],
            "reused": false,
            "customers": [
              "DARPA"
            ],
            "nationality": "United States",
            "manufacturer": "SSTL",
            "payload_type": "Satellite",
            "payload_mass_kg": 20,
            "payload_mass_lbs": 43,
            "orbit": "LEO",
            "orbit_params": {
              "reference_system": "geocentric",
              "regime": "low-earth",
              "longitude": "test",
              "semi_major_axis_km": "test",
              "eccentricity": "test",
              "periapsis_km": 400,
              "apoapsis_km": 500,
              "inclination_deg": 39,
              "period_min": "test",
              "lifespan_years": "test",
              "epoch": "test",
              "mean_motion": "test",
              "raan": "test",
              "arg_of_pericenter": "test",
              "mean_anomaly": "test"
            }
          }
        ]
      },
      "fairings": {
        "reused": false,
        "recovery_attempt": false,
        "recovered": false,
        "ship": "test"
      }
    },
    "ships": [],
    "telemetry": {
      "flight_club": "test"
    },
    "launch_site": {
      "site_id": "kwajalein_atoll",
      "site_name": "Kwajalein Atoll",
      "site_name_long": "Kwajalein Atoll Omelek Island"
    },
    "launch_success": false,
    "launch_failure_details": {
      "time": 33,
      "altitude": "test",
      "reason": "merlin engine failure"
    },
    "links": {
      "mission_patch": "https://images2.imgbox.com/40/e3/GypSkayF_o.png",
      "mission_patch_small": "https://images2.imgbox.com/3c/0e/T8iJcSN3_o.png",
      "reddit_campaign": "test",
      "reddit_launch": "test",
      "reddit_recovery": "test",
      "reddit_media": "test",
      "presskit": "test",
      "article_link": "https://www.space.com/2196-spacex-inaugural-falcon-1-rocket-lost-launch.html",
      "wikipedia": "https://en.wikipedia.org/wiki/DemoSat",
      "video_link": "https://www.youtube.com/watch?v=0a_00nJ_Y88",
      "youtube_id": "0a_00nJ_Y88",
      "flickr_images": []
    },
    "details": "Engine failure at 33 seconds and loss of vehicle",
    "static_fire_date_utc": "2006-03-17T00:00:00.000Z",
    "static_fire_date_unix": 1142553600,
    "timeline": {
      "webcast_liftoff": 54
    },
    "crew": "test"
  }


  export const mockState = {
    list: [flight],
    search: "",
    loader: false,
    filter: {
      launchDate: "2022-05-20",
      launchStatus: "Success",
      upComing: false,
    },
  } as IState;