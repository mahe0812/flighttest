import FilterComponentView, {
  IFilterComponentView,
} from "../presentational/filterComponentView";
import { useSelector, useDispatch } from "react-redux";
import { setFilterValues } from "../../../actions/flightListAction";
import { IRootState } from "../../../store/store";
import { Dispatch } from "redux";

const FilterContainer = () => {
  const dispatch:Dispatch<any> = useDispatch();

  const { launchDate, launchStatus, upComing } = useSelector((state: IRootState) => ({
    launchDate: state && state.Flight && state.Flight.filter.launchDate,
    launchStatus: state && state.Flight && state.Flight.filter.launchStatus,
    upComing: state && state.Flight && state.Flight.filter.upComing,
  }));

  const updateFiltervalues = (filterval: IFilterComponentView) => {
    dispatch(setFilterValues(filterval));
  };

  return (
    <FilterComponentView
      updateFiltervalues={updateFiltervalues}
      launchDate={launchDate}
      launchStatus={launchStatus}
      upComing={upComing}
    />
  );
};

export default FilterContainer;
