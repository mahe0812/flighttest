import React from "react";
import SearchComponentView from "../presentational/searchComponentView";
import { useDispatch, useSelector } from "react-redux";
import { setFlightSearch } from "../../../actions/flightListAction";
import { IRootState } from "../../../store/store";
import { Dispatch } from "redux";

const SearchContainer = () => {
  const dispatch: Dispatch<any> = useDispatch();

  const searchVal = useSelector(
    (selector: IRootState) => selector.Flight && selector.Flight.search
  );

  const updateSearchValue = (val: string) => {
    dispatch(setFlightSearch(val));
  };
  const clearSearch = () => {
    dispatch(setFlightSearch(""));
  };
  return (
    <SearchComponentView
      value={searchVal}
      onCancelSearch={clearSearch}
      updateSearchValue={updateSearchValue}
    />
  );
};

export default SearchContainer;
