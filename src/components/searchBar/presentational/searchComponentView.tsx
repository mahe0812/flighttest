import SearchBar from "material-ui-search-bar";

interface ISearchComponentView {
  value: string;
  updateSearchValue : (val: string) => void;
  onCancelSearch: () => void;
}

const SearchComponentView = (props: ISearchComponentView) => {
  const { value, updateSearchValue, onCancelSearch } = props;
  return (
    <SearchBar
      value={value}
      onChange={(newValue) => updateSearchValue(newValue)}
      onCancelSearch={onCancelSearch}
    />
  );
};

export default SearchComponentView;
