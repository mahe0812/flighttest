import React, { memo } from "react";
import {
  FormControl,
  InputLabel,
  Select,
  MenuItem,
} from "../../../utils/uiComponents";
import { filterTypeEnum } from "../filterTypeEnum";

export interface IFilterComponentView {
  launchDate: string;
  launchStatus: string;
  upComing: boolean;
}

interface IFilterComponentViewProps extends IFilterComponentView {
  updateFiltervalues: (filterVal: IFilterComponentView) => void;
}
const FilterComponentView = memo((props: IFilterComponentViewProps) => {
  const { launchDate = "", updateFiltervalues, launchStatus = "", upComing = false } = props;
  const handleLaunchDateChange = (_e: any, r: any) => {
    updateFiltervalues({
      launchDate: r.props.value,
      launchStatus,
      upComing,
    });
  };
  const handleLaunchStatusChange = (_e: any, r: any) => {
    updateFiltervalues({
      launchDate,
      launchStatus: r.props.value,
      upComing,
    });
  };
  const handleUpComingChange = (_e: any, r: any) => {
    updateFiltervalues({
      launchDate,
      launchStatus,
      upComing: r.props.value,
    });
  };
  return (
    <div>
      <FormControl variant="filled" className="filter-item">
        <InputLabel id="demo-simple-select-filled-label">
          Launch Date
        </InputLabel>
        <Select
          labelId="demo-simple-select-filled-label"
          id="demo-simple-select-filled"
          value={launchDate}
          onChange={handleLaunchDateChange}
        >
          <MenuItem value="">
            <em>None</em>
          </MenuItem>
          <MenuItem value={filterTypeEnum.LAST_WEEK}>Last Week</MenuItem>
          <MenuItem value={filterTypeEnum.LAST_MONTH}>Last Month</MenuItem>
          <MenuItem value={filterTypeEnum.LAST_YEAR}>Last Year</MenuItem>
        </Select>
      </FormControl>
      <FormControl variant="filled" className="filter-item">
        <InputLabel id="demo-simple-select-filled-label">
          Launch Status
        </InputLabel>
        <Select
          labelId="demo-simple-select-filled-label"
          id="demo-simple-select-filled"
          value={launchStatus}
          onChange={handleLaunchStatusChange}
        >
          <MenuItem value="">
            <em>None</em>
          </MenuItem>
          <MenuItem value={filterTypeEnum.FAILURE}>Failure</MenuItem>
          <MenuItem value={filterTypeEnum.SUCCESS}>Success</MenuItem>
        </Select>
      </FormControl>
      <FormControl variant="filled" className="filter-item">
        <InputLabel id="demo-simple-select-filled-label">
          is upComing
        </InputLabel>
        <Select
          labelId="demo-simple-select-filled-label"
          id="demo-simple-select-filled"
          value={upComing}
          onChange={handleUpComingChange}
        >
          <MenuItem value="">
            <em>None</em>
          </MenuItem>
          <MenuItem value={filterTypeEnum.YES}>Yes</MenuItem>
          <MenuItem value={filterTypeEnum.NO}>No</MenuItem>
        </Select>
      </FormControl>
    </div>
  );
});

export default FilterComponentView;
