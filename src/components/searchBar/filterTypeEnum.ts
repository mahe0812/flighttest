export enum filterTypeEnum {
    LAST_WEEK = "LAST_WEEK",
    LAST_MONTH = "LAST_MONTH",
    LAST_YEAR = "LAST_YEAR",
    FAILURE = "FAILURE",
    SUCCESS = "SUCCESS",
    YES = "YES",
    NO = "NO"
}