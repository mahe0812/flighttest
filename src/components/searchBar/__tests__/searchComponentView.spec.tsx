import { shallow } from "enzyme";
import SearchComponentView from "../presentational/searchComponentView";

const searchViewProps = {
    value: "test",
    updateSearchValue: jest.fn(),
    onCancelSearch: jest.fn()
};

describe("expect SearchComponentView test", () => {
  it("shallow check of the SearchComponentView", () => {
    const wrapper = shallow(<SearchComponentView {...searchViewProps} />);
    expect(wrapper).toBeDefined();
  });
});
