import { shallow} from "enzyme";
import FilterComponentView from "../presentational/filterComponentView";
import {Select} from "../../../utils/uiComponents";

const filterComponentViewProps = {
  updateFiltervalues: jest.fn(),
  launchDate: "2020-05-24",
  launchStatus: "Success",
  upComing: false,
};

describe("expect navBar test", () => {
  it("shallow check of the filterComponentView", () => {
    const wrapper = shallow(
      <FilterComponentView {...filterComponentViewProps} />
    );
    expect(wrapper.find(Select).length).toEqual(3);
    expect(wrapper).toBeDefined();
  });
});
