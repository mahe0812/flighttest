import { shallow } from "enzyme";
import { mockState } from "../../../utils/mockData";
import SearchContainer from "../container/searchContainer";

jest.mock("react-redux", () => {
  const ActualReactRedux = jest.requireActual("react-redux");
  return {
    ...ActualReactRedux,
    useSelector: jest.fn().mockImplementation(() => {
      return mockState;
    }),
    useDispatch: jest.fn()
  };
});

describe("expect SearchContainer test", () => {
  it("shallow check of the SearchContainer", () => {
    const wrapper = shallow(<SearchContainer />);
    expect(wrapper).toBeDefined();
  });
});
