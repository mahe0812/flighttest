import { shallow } from "enzyme";
import { mockState } from "../../../utils/mockData";
import FilterContainer from "../container/filterContainer";

jest.mock("react-redux", () => {
  const ActualReactRedux = jest.requireActual("react-redux");
  return {
    ...ActualReactRedux,
    useSelector: jest.fn().mockImplementation(() => {
      return mockState;
    }),
    useDispatch: jest.fn()
  };
});

describe("expect filtercontainer test", () => {
  it("shallow check of the FilterContainer", () => {
    const wrapper = shallow(<FilterContainer />);
    expect(wrapper).toBeDefined();
  });
});
