import React from "react";
import { Grid } from "../../utils/uiComponents";
import SearchContainer from "./container/searchContainer";
import FilterContainer from "./container/filterContainer";

const SearchBar = () => {
  return (
    <div className="search-container">
      <Grid container spacing={3}>
        <Grid item xs={3}>
          <SearchContainer />
        </Grid>
        <Grid item xs={9}>
          <FilterContainer />
        </Grid>
      </Grid>
    </div>
  );
};

export default SearchBar;
