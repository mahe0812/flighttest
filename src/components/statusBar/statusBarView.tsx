import React from "react";

interface IStatusBarView {
    listLength: number;
}
const StatusBarview = (props:IStatusBarView) => {
    return (
    <div className="status-bar"><span>List length: {props.listLength}</span></div>
    )
}

export default StatusBarview;