import { shallow } from "enzyme";
import StatusBarView from "../statusBarView";

const statusBarProps = {
  listLength: 10,
};
describe("expect navBar test", () => {
  it("shallow check of the NavBar", () => {
    const wrapper = shallow(<StatusBarView {...statusBarProps} />);
    expect(wrapper).toBeDefined();
  });
});
