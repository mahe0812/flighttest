import { ErrorBoundary } from "react-error-boundary";
import NavBar from "./navBar/navBar";
import CardListContainer from "./list/container/cardListContainer";
import SearchBar from "./searchBar/searchBar";

function ErrorFallback({ error }: any) {
  return (
    <div>
      <p>Something went wrong:</p>
      <pre>{error.message}</pre>
    </div>
  );
}

const Root = () => {
  return (
    <ErrorBoundary FallbackComponent={ErrorFallback}>
      <div className="body-layout">
        <NavBar />
        <SearchBar />
        <CardListContainer />
      </div>
    </ErrorBoundary>
  );
};

export default Root;
