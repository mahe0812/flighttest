import { shallow } from "enzyme";
import CardList from "../presentational/cardList";
import { flight } from "../../../utils/mockData";
import FlightCard from "../../card/card";

const list = [flight];
describe("expect card test", () => {
  it("shallow test with undefined list", () => {
    const wrapper = shallow(<CardList flightList={undefined} />);
    expect(wrapper).toBeDefined();
  });
  it("shallow test with empty list", () => {
    const wrapper = shallow(<CardList flightList={[]} />);
    expect(wrapper.find(".empty-list").length).toEqual(1);
    expect(wrapper).toBeDefined();
  });
  it("shallow test with data list", () => {
    const wrapper = shallow(<CardList flightList={list} />);
    expect(wrapper.find(FlightCard).length).toEqual(1);
    expect(wrapper).toBeDefined();
  });
});
