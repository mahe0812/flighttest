import { shallow } from "enzyme";
import { mockState } from "../../../utils/mockData";
import CardListContainer from "../container/cardListContainer";

jest.mock("react-redux", () => {
  const ActualReactRedux = jest.requireActual("react-redux");
  return {
    ...ActualReactRedux,
    useSelector: jest.fn().mockImplementation(() => {
      return mockState;
    }),
    useDispatch: jest.fn(),
  };
});

describe("expect card container test", () => {
  it("shallow test of", () => {
    const wrapper = shallow(<CardListContainer />);
    expect(wrapper).toBeDefined();
  });
});
