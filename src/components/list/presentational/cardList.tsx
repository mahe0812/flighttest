import React, { memo } from "react";
import { Grid } from "../../../utils/uiComponents";
import FlightCard from "../../card/card";
import { ICardModel } from "../../card/iCardModel";

interface ICardList {
  flightList: Array<ICardModel> | undefined;
}

const CardList = memo((props: ICardList) => {
  const { flightList } = props;
  if (Array.isArray(flightList) && !flightList.length) {
    return (
      <Grid container xs={12} spacing={2}>
        <Grid item xs={6} sm={3}>
          <div className="empty-list">No Results matching your query</div>
        </Grid>
      </Grid>
    );
  }
  return (
    <div>
      <Grid container xs={12} spacing={2}>
        {(flightList || []).map((flight, index) => (
          <Grid item xs={6} sm={3} key={index}>
            <FlightCard flight={flight} />
          </Grid>
        ))}
      </Grid>
    </div>
  );
});

export default CardList;
