import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getFligtListAction } from "../../../actions/flightListAction";
import { filteredQueryList } from "../../../utils/commonUtils";
import { CircularProgress } from "../../../utils/uiComponents";
import StatusBarview from "../../statusBar/statusBarView";
import CardList from "../presentational/cardList";
import { Dispatch } from "redux";
import { IRootState } from "../../../store/store";

const CardListContainer = () => {
  const dispatch: Dispatch<any> = useDispatch();

  const stateSelectors: any = useSelector((state: IRootState) => ({
    totalFlightList: state && state.Flight && state.Flight.list,
    searchString: state && state.Flight && state.Flight.search,
    loader: state && state.Flight && state.Flight.loader,
    launchDate: state && state.Flight && state.Flight.filter.launchDate,
    launchStatus: state && state.Flight && state.Flight.filter.launchStatus,
    upComing: state && state.Flight && state.Flight.filter.upComing,
  }));

  const { searchString, totalFlightList, loader , launchDate, launchStatus, upComing} = stateSelectors;

  const [queriedFlightList, setFlightList] = useState(totalFlightList);

  useEffect(() => {
    dispatch(getFligtListAction());
  }, [dispatch]);

  useEffect(() => {
    if (totalFlightList) {
      setFlightList(totalFlightList);
    }
  }, [totalFlightList]);

  useEffect(() => {
    if(!searchString && !launchStatus && !launchDate && !upComing ){
      setFlightList(totalFlightList);
    } else {
      const queriedList = filteredQueryList(totalFlightList, {
        q: searchString,
        searchKey: "rocket_name",
        launchDate,
        launchStatus,
        upComing
      });
      setFlightList(queriedList);
    }
  }, [searchString, launchDate, launchStatus, upComing, totalFlightList]);

  return loader ? (
    <div className="loader">
      <CircularProgress />
    </div>
  ) : (
    <>
    <StatusBarview listLength={queriedFlightList?.length}/>
    <CardList flightList={queriedFlightList} />
    </>
  );
};

export default CardListContainer;
