import { shallow } from "enzyme";
import NavBar from "../navBar";

describe("expect navBar test", () => {
  it("shallow check of the NavBar", () => {
    const wrapper = shallow(<NavBar />);
    expect(wrapper).toBeDefined();
    expect(wrapper.text()).toContain("Home");
  });
});
