import React from "react";

const NavBar = () => {
  return (
    <header className="header-fixed">
      <div className="header-limiter">
        <h1>Mahesh Test</h1>
        <nav>
          <button>Home</button>
        </nav>
      </div>
    </header>
  );
};

export default NavBar;
