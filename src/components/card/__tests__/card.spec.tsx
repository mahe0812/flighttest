import { shallow } from "enzyme";
import { flight } from "../../../utils/mockData";
import FlightCard from "../card";
import {Typography} from "../../../utils/uiComponents";

describe("expect card test", () => {
  it("shallow test", () => {
    const wrapper = shallow(<FlightCard flight={flight} />);
    expect(wrapper).toBeDefined();
    expect(wrapper.find("#flightCard")).toBeDefined();
    expect(wrapper.find("[variant='body2']").length).toEqual(2);
  });
  it("test material components", () => {
    const wrapper = shallow(<FlightCard flight={flight} />);
    expect(wrapper.find(Typography).length).toEqual(5);
  });
});
