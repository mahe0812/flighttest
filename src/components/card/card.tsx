import moment from "moment";
import React from "react";
import { Card, CardContent, Typography } from "../../utils/uiComponents";
import { ICardModel } from "./iCardModel";

const FlightCard = (props: { flight: ICardModel }) => {
  const { flight } = props;
  return (
    <Card id="flightCard" className="card-container">
      <CardContent>
        <Typography className="card-title" color="textSecondary" gutterBottom>
          {flight["mission_name"]}
        </Typography>
        <Typography variant="h5" component="h2">
          {flight.rocket["rocket_name"]}
        </Typography>
        <Typography className="card-pos" color="textSecondary">
          {moment(flight["launch_date_local"]).calendar()}
        </Typography>
        <Typography variant="body2" component="p">
          Is Success? {flight["launch_success"] ? "Yes" : "No"}
        </Typography>
        <Typography variant="body2" component="p">
          Is upcoming? {flight["upcoming"] ? "Yes" : "No"}
        </Typography>
      </CardContent>
    </Card>
  );
};

export default FlightCard;
