import { applyMiddleware, createStore, compose } from "redux";
import createSagaMiddleware from "redux-saga";
import commonRootReducer from "../reducers";
import { IState } from "../reducers/flightListReducer";
import commonRootSaga from "../sagas";

declare global {
  interface Window {
    __REDUX_DEVTOOLS_EXTENSION_COMPOSE__: any;
  }
}

export interface IRootState {
  Flight: IState;
}

const sagaMiddleWare = createSagaMiddleware();

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const appStore: any = createStore(
  commonRootReducer,
  composeEnhancers(applyMiddleware(sagaMiddleWare))
);
appStore.sagaTask = sagaMiddleWare.run(commonRootSaga);

export default appStore;
