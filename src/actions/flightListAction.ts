import { IFilterComponentView } from "../components/searchBar/presentational/filterComponentView";

export const getFligtListAction = () => ({
  type: "GET_FLIGHT_LIST",
});

export const setFlightSearch = (val: string) => ({
  type: "SET_SEARCH_VAL",
  payload: val,
});

export const setFilterValues = (val:IFilterComponentView) => ({
  type: "SET_FILTER_VAL",
  payload: val
});
