import { call, put, takeLatest } from "redux-saga/effects";
import { fetchFlightList } from "../api/flightAPI";

export function* FlightListSaga() {
  yield takeLatest("GET_FLIGHT_LIST", getFlightList);
}

function* getFlightList(): any {
  yield put({ type: "SET_LOADER_STATUS", payload: true });
  let response: any = [];

  response = yield call(() => fetchFlightList());
  if(response.error){
    response = [];
  }
  yield put({ type: "SET_FLIGHT_LIST", payload: response });
  yield put({ type: "SET_LOADER_STATUS", payload: false });
}
