import { all } from "redux-saga/effects";
import {FlightListSaga} from "./flightListSaga";

function* rootSaga(){
    yield all([FlightListSaga()]);
}

export default rootSaga;