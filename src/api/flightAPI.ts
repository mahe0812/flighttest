import { ICardModel } from "../components/card/iCardModel";
import { getAPIURL } from "../utils/apiUtils";
import APIGateway from "./apiGateway";


export const fetchFlightList = (): Promise<ICardModel[]> => {
  const baseURL = getAPIURL();
  return APIGateway.get(baseURL + "/launches");
};
