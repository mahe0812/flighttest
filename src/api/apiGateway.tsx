import Axios from "axios";

class APIGateway {
  static get: any = (url: string) => {
    return Axios.get(url)
      .then((response) => {
        return response.data;
      })
      .catch((e) => {
        return { error: "FAILED TO LOAD DATA" };
      });
  };
}

export default APIGateway;
