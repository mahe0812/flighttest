import React from 'react';
import Root from "./components/root";
import './App.scss';

function App() {
  return (
    <Root/>
  );
}

export default App;
